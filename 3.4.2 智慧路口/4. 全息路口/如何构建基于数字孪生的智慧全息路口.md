- [如何构建基于数字孪生的智慧全息路口 (qq.com)](https://mp.weixin.qq.com/s/abnRyxYTIlkP3Ii9olYT7Q)

**简介：** 全息路口是基于数字孪生技术，将城市道路上的全要素进行数字化还原，进而为交通治理提供一体化解决措施，是为交通精细化治理而生的一款产品。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dI86HJ8IoPt2UBXbzzicVk49QvrmOGSLByZWVmrsAiapKNulTYW8HSbpA/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

全息路口是基于数字孪生技术，将城市道路上的全要素进行数字化还原，进而为交通治理提供一体化解决措施，是为交通精细化治理而生的一款产品。

对路口交通进行精细化治理，首先要获得路口的交通信息，比如车行状况、人行状况、存在何种交通问题、应该采用何种治理措施等，将路口客观存在的交通对象转换成系统可理解的数据，这就是基于数字孪生技术进行全息路口构建的过程。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4do2MJDrbhDea1dy4avs5iaN7b8GxxN85QicwnPLfo0P7gznTnpm7Ae9pA/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

全息路口的应用场景主要有以下四个方面：

① 效率：服务于交通效率的提升。比如信号灯配置优化，如果一个方向为红灯，但交叉方向并没有车，这说明路口的信号灯配置存在问题，需要进一步优化。此外还有路口的交通组织、智能可变车道等，都可以提升路口效率。

② 交通安全：路口存在车辆与非机动车互相抢道的问题，存在极大的安全隐患，除此之外还有交通秩序研判、交通事件报警等。

③ 交通管理：交通管理者时常需要保障一些特殊任务，比如警卫保障任务、公交优先以及重点车辆管控。

④ 出行服务：比如按照最优路线对行人进行诱导，比如针对路口存在的安全问题为车辆提供预警，或在车里即可获取前方路口红绿灯情况。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dUBxxEGiaTmsZB96iaXoDfticFZUfTgCI4xMsA9dP3QYNYPic8fsNlfG6BA/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

引入全息路口前，行业存在诸多痛点，主要有三个方面：

① 数据不全面：像摄像头、球机设备都只是视频数据，需要人为查看才能明确问题所在，另外设备和数据的准确度会随着使用逐渐降低甚至不可用。数据传到云端，实时性和延迟性都是很大的问题。

② 优化不精细：当前大部分路口采用固定配时方案，但交通流是实时的，固定的配置方案无法与实时的交通流匹配，交通效率低下。

③ 措施不丰富：路口治理是一个综合治理的过程，通过单一的、相互独立的手段难以达到最优效果。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4d8VicIcdqkeHS401BILWJsx0pLJ9ITM2mGE0Tk3lXWs0UoPn4ibAcfqmg/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

阿里云全息路口产品采用云边结合的架构，通过标准化的数据服务来支撑业务的灵活拓展。边端接入了前端感知设备的数据，数据会通过边缘计算来进行第一步分析，再到云端进行决策性的处理。

边端接入的数据主要有三类，第一类是感知设备的数据，比如路口的雷视一体机、卡口、电子警察、激光雷达等；第二类是控制设备的数据，比如路口的摄像机、诱导屏、可变车道等；第三类是用户数据，比如车载OBU、高德 App 用户。

边端接入的数据首先在边端进行处理，将路口的非结构化数据转为结构化数据，再对数据进行初步融合清洗，比如雷达、视频数据不准或重复等问题，最后对路口进行实时优化。优化的实时性决定了路口优化的效率，全息路口产品可实现秒级优化。

数据经边端处理之后进入云端。云端分为两个层次，分别是云端中台层和全息路口，其中中台层主要包含三个部分：

① 运行中心：路口所有设备情况、故障情况、数据链路情况都给会通过数据中心进行监控来。

② 算法引擎：负责路口端的实时优化，多路口、多区域的情况需要通过云端云边一体进行优化。另外，跨设备的轨迹处理也需要在云端进行处理。

③ 数据指标计算：所有计算的数据都会通过标准的数据服务提供给全息路口，实现配置优化、事件报警、分析研判、车路协同等业务。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dylHuibtU2OeIicSsBVt1r8Zc1QFRZNkG9Q63fvsic8hp4EEodKIj64VQg/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

边缘融合感知能力最大可以接入 16 路 1080p 高清摄像机，通过内置 AR 算法对雷达、视频以及互联网的数据进行综合分析计算，最终产出三个方面的数据：

① 交通参数检测：交通中常用的流量、排队长度、停车次数、车头间距、车头时距、空间占有率等都可通过视频 AI 分析来得到。

② 交通事件检测：路口发生事件后，策略都会基于事件而改变。边缘融合感知能力能够对常规事件进行识别检测，比如路口违法停车、车辆压线、不按导向车道行驶、交通事故、拥堵排队溢出等。

③ 轨迹定位识别：可以实现车道级的定位，将每一辆车挂载上车牌，真实还原到高清地图上，做到路口的全息还原。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dg6191TrCIHCfnLrteGVEia9DUeveibp6SPkFsYB4z5px9lCu4n17CIrQ/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

上图为三个路口的示意图。每路口都有边缘计算的设备，单个路口的边缘计算设备可以完成本路口的优化。针对一条连续的路口，如果只对单独的路口进行优化，往往无法达到最优效果，而是需要对各路口进行有效协同。

在我们的方案里，三个路口的边际可以自行组成网络，设备充当主节点，做三个路口之间的统筹协调，进而实现路口协调和实时优化的紧密结合。

单独的信号灯优化对于路口的治理而言远远不够，路口的治理是人、车、路、环境综合的治理过程。

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dvnlJ7lSYOgtNocdWs6MeZ7YvVl4cmd3ACc1NTCIdfYYegRry6zTvyw/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

通过全息路口实时感知路口车辆的排队数据，可以通过智能计算路口的车道划分来实时改变车道属性，进而提高通行效率；针对路上的车辆，可以根据信号灯配置情况和信号方案情况，为其推荐最佳行驶速度，尽量实现不停车通过路口；此外，还可以向车辆推荐行驶路径，提前实现车辆换道，规避拥堵。

通过以上几种时空一体化的手段相结合，可以实现优秀的路口综合治理效果。

全息路口具备以下6个优秀特性：

![图片](https://mmbiz.qpic.cn/mmbiz_png/z1TmicYcaevFA1vohTXicJ4aHjM9jrZe4dUbsibKtchDtxgax8SLk4ujrK8wFyVlM6bWMSnUvEOgfIlI2vWPHbicgw/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)

① 云边一体自动运维：针对路口的几百个设备，无须挨个运维。可以通过云边一体运维，自动发现路口的设备，自动对设备进行批量的远程升级，自动监测设备的报警情况。

② 全方位内置交通AI：不管是视频的智能分析还是路口的实时优化，都内嵌于边缘计算，可以实现开箱即用。

③ 交通控制协议全打通：设备内置了交通协议市场，可以针对不同品牌、型号的设备选择不同的协议进行对接。

④ 高性能高鲁棒高性价比：可以在零下 40℃到 80 ℃工作。

⑤ 二三维全息路网应用SDK：产品与高德地图进行了紧密结合，可以进行二三维全息路网的应用 SDK ，往外透出。

⑥ 支持多种接口输入：支持 HDMI/VGA/RJ45/并行串口等接口输入。